FROM tomcat:9.0.5
MAINTAINER dockerhub@agilob.net

COPY tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml
COPY context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml

EXPOSE 8080/tcp
