#!/bin/bash -e

REPO="tomcat_starter"

docker build . | tee .log.txt
TAG=`cat .log.txt | tail -n1 | awk 'NF>1{printf $NF}'`
rm .log.txt

docker tag $TAG agilob/$REPO:latest
docker push agilob/$REPO

#docker run --restart=always -it -p 8080:8080 $TAG
